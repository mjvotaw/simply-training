CREATE TABLE custom_meter (
    username TEXT NOT NULL REFERENCES account (name),
    steps_hash CHAR(16) REFERENCES steps (hash),
    trail_hash CHAR(16) REFERENCES trail (hash),
    meter INTEGER NOT NULL CHECK (meter > 0),

    CHECK ((steps_hash IS NULL) != (trail_hash IS NULL)),
    UNIQUE (username, steps_hash),
    UNIQUE (username, trail_hash)
);
CREATE INDEX ON custom_meter (username);
CREATE INDEX ON custom_meter (steps_hash);
CREATE INDEX ON custom_meter (trail_hash);
CREATE INDEX ON custom_meter (meter);
