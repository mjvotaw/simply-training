#!/usr/bin/env python3
import getopt
import json
import math
import os
import re
import sys
from functools import lru_cache
from xml.etree import ElementTree


WEIGHTS = {
    'ITG': {
        'w1': 5,
        'w2': 4,
        'w3': 2,
        'w4': 0,
        'w5': -6,
        'miss': -12,
        'let_go': 0,
        'held': 5,
        'hit_mine': -6,
    },
    'FA+': {
        'w1': 5,
        'w2': 5,
        'w3': 4,
        'w4': 2,
        'w5': 0,
        'miss': -12,
        'let_go': 0,
        'held': 5,
        'hit_mine': -6,
    },
}


# HACK: The python3 round() implementation rounds .5 to even, but we want to
# round .5 up in order to be in line with the behaviour of the online
# converter. Also, rounding up makes more sense for our usecases.
def round(n):
    if n - math.floor(n) < 0.5:
        return math.floor(n)
    return math.ceil(n)


# XXX: unreliable for failed scores, 0% and changed charts
def detect_game_mode(score, steps):
    max_grade_points = (
        steps['Radar']['TapsAndHolds'] +
        steps['Radar']['Holds'] +
        steps['Radar']['Rolls']
    ) * 5

    min_points = math.floor(max_grade_points * score['PercentDP'])
    max_points = math.ceil(max_grade_points * (score['PercentDP'] + 0.0001))

    for game_mode, params in WEIGHTS.items():
        grade_points = (
            score['TapNoteScores']['W1'] * params['w1'] +
            score['TapNoteScores']['W2'] * params['w2'] +
            score['TapNoteScores']['W3'] * params['w3'] +
            score['TapNoteScores']['W4'] * params['w4'] +
            score['TapNoteScores']['W5'] * params['w5'] +
            score['TapNoteScores']['Miss'] * params['miss'] +
            score['HoldNoteScores']['LetGo'] * params['let_go'] +
            score['HoldNoteScores']['Held'] * params['held'] +
            score['TapNoteScores']['HitMine'] * params['hit_mine']
        )
        if grade_points < 0:
            grade_points = 0

        if min_points <= grade_points < max_points:
            return game_mode

    return 'ITG'


class Cache:
    def __init__(self, cachedir):
        self._dir = cachedir

    @lru_cache(maxsize=None)
    def get_song_data(self, song_dir):
        filename = song_dir.strip('/').replace('/', '_')
        try:
            with open(os.path.join(self._dir, 'Songs', filename)) as f:
                text = f.read()
        except FileNotFoundError:
            raise KeyError(song_dir)

        song = {
            'Dir': '/' + song_dir,
            'Group': song_dir.split('/')[-3],
            'RandomBPM': False,
        }
        steps = {}
        current_steps = None

        for m in re.finditer(r'^\s*#(?P<key>\w+):(?P<value>([^;\\]|\\.)*);', text, re.MULTILINE):
            key = m.group('key').lower()
            value = re.sub(r'\\(.)', r'\1', m.group('value'))

            if key == 'title':
                song['Title'] = value

            elif key == 'subtitle':
                if value:
                    song['SubTitle'] = value

            elif key == 'artist':
                if value:
                    song['Artist'] = value

            elif key == 'titletranslit':
                if value:
                    song['TranslitTitle'] = value

            elif key == 'subtitletranslit':
                if value:
                    song['TranslitSubTitle'] = value

            elif key == 'artisttranslit':
                if value:
                    song['TranslitArtist'] = value

            elif key == 'genre':
                if value:
                    song['Genre'] = value

            elif key == 'displaybpm':
                if value == '*':
                    song['RandomBPM'] = True
                else:
                    if ':' in value:
                        low, high = [round(float(bpm)) for bpm in value.split(':')]
                        if low > high:
                            low, high = high, low
                    else:
                        bpm = round(float(value))
                        low = bpm
                        high = bpm

                    if low > 0 and high > 0:
                        song['BPM'] = [low, high]
                    else:
                        print(f'WARNING: negative bpm: {low}-{high}, ignored', file=sys.stderr)

            elif key == 'bpms':
                bpms = list(map(float, (part.split('=')[1] for part in value.split(','))))
                bpms = [bpm for bpm in bpms if bpm > 0]

                if 'BPM' not in song:
                    low = round(min(bpms))
                    high = round(max(bpms))
                    song['BPM'] = [low, high]

            elif key == 'musiclength':
                song['MusicLengthSeconds'] = round(float(value))

            elif key == 'notedata':
                if current_steps is not None:
                    steps[current_steps['StepsType'], current_steps['Difficulty']] = current_steps

                current_steps = {}

            elif key == 'difficulty':
                assert current_steps is not None
                assert value in ('Beginner', 'Easy', 'Medium', 'Hard', 'Challenge', 'Edit')
                current_steps['Difficulty'] = value

            elif key == 'meter':
                assert current_steps is not None
                current_steps['Meter'] = int(value)

            elif key == 'stepstype':
                assert current_steps is not None
                current_steps['StepsType'] = value

            elif key == 'credit':
                if current_steps is not None:
                    current_steps['AuthorCredit'] = value

            elif key == 'description':
                assert current_steps is not None
                current_steps['Description'] = value

            elif key == 'radarvalues':
                assert current_steps is not None
                vals = [float(v) for v in value.split(',')]
                assert len(vals) % 14 == 0

                current_steps['Radar'] = {
                    'Notes': int(vals[5]),
                    'TapsAndHolds': int(vals[6]),
                    'Jumps': int(vals[7]),
                    'Holds': int(vals[8]),
                    'Mines': int(vals[9]),
                    'Hands': int(vals[10]),
                    'Rolls': int(vals[11]),
                }

        if current_steps is not None:
            steps[current_steps['StepsType'], current_steps['Difficulty']] = current_steps

        if 'TranslitTitle' not in song:
            song['TranslitTitle'] = song['Title']
        if 'TranslitSubTitle' not in song and 'SubTitle' in song:
            song['TranslitSubTitle'] = song['SubTitle']
        if 'TranslitArtist' not in song and 'Artist' in song:
            song['TranslitArtist'] = song['Artist']

        return song, steps


def parse_score(high_score):
    tns = high_score.find('TapNoteScores')
    hns = high_score.find('HoldNoteScores')
    radar = high_score.find('RadarValues')

    taps_and_holds = int(radar.findtext('TapsAndHolds'))
    jumps = int(radar.findtext('Jumps'))
    notes = radar.findtext('Notes')
    if notes is not None:
        notes = int(float(notes))
    else:
        notes = taps_and_holds + jumps

    return {
        'Guid': high_score.findtext('Guid'),
        'Grade': high_score.findtext('Grade'),
        'Score': int(high_score.findtext('Score')),
        'PercentDP': float(high_score.findtext('PercentDP')),
        'SurviveSeconds': float(high_score.findtext('SurviveSeconds')),
        'MaxCombo': int(high_score.findtext('MaxCombo')),
        'Modifiers': high_score.findtext('Modifiers'),
        'DateTime': high_score.findtext('DateTime'),
        'PlayerGuid': high_score.findtext('PlayerGuid') or high_score.findtext('MachineGuid'),
        'Disqualified': int(high_score.findtext('Disqualified')) > 0,
        'TapNoteScores': {
            'W1': int(tns.findtext('W1')),
            'W2': int(tns.findtext('W2')),
            'W3': int(tns.findtext('W3')),
            'W4': int(tns.findtext('W4')),
            'W5': int(tns.findtext('W5')),
            'Miss': int(tns.findtext('Miss')),
            'HitMine': int(tns.findtext('HitMine')),
            'AvoidMine': int(tns.findtext('AvoidMine')),
            'CheckpointMiss': int(tns.findtext('CheckpointMiss')),
            'CheckpointHit': int(tns.findtext('CheckpointHit')),
        },
        'HoldNoteScores': {
            'LetGo': int(hns.findtext('LetGo')),
            'Held': int(hns.findtext('Held')),
            'MissedHold': int(hns.findtext('MissedHold') or 0),
        },
        'Radar': {
            'Notes': notes,
            'TapsAndHolds': taps_and_holds,
            'Jumps': jumps,
            'Holds': int(radar.findtext('Holds')),
            'Mines': int(radar.findtext('Mines')),
            'Hands': int(radar.findtext('Hands')),
            'Rolls': int(radar.findtext('Rolls')),
        },
    }


def convert_file(path, cache, player_guid):
    root = ElementTree.parse(path)
    converted = []

    template = {
        'Version': 1,
        'Theme': 'convert.py',
        'ThemeVersion': '1.1',
        'ProductID': 'convert.py',
        'ProductVersion': '1.1',
        'MachineGuid': root.findtext('MachineGuid'),
    }

    for element in root.findall('./RecentSongScores/HighScoreForASongAndSteps'):
        if player_guid is not None:
            if element.findtext('./HighScore/PlayerGuid') != player_guid:
                continue

        song_dir = element.find('./Song').attrib['Dir']
        steps_type = element.find('./Steps').attrib['StepsType']
        difficulty = element.find('./Steps').attrib['Difficulty']

        try:
            song_info, steps_infos = cache.get_song_data(song_dir)
            steps_info = steps_infos[steps_type, difficulty]
        except KeyError:
            print(f'{path}: {song_dir} not in cache, skipped score')
            continue

        data = template.copy()
        data['Score'] = parse_score(element.find('./HighScore'))
        data['Song'] = song_info
        data['Steps'] = steps_info
        data['GameMode'] = detect_game_mode(data['Score'], data['Steps'])

        converted.append(data)

    for element in root.findall('./RecentCourseScores/HighScoreForACourseAndTrail'):
        if player_guid is not None:
            if element.findtext('./HighScore/PlayerGuid') != player_guid:
                continue

        print(f'{path}: course score skipped')

    return converted


def convert_files(smdir, outdir, player_guid):
    cache = Cache(os.path.join(smdir, 'Cache'))
    uploaddir = os.path.join(smdir, 'Save', 'Upload')

    for filename in sorted(os.listdir(uploaddir)):
        if not filename.endswith('.xml'):
            continue

        scores = convert_file(os.path.join(uploaddir, filename), cache, player_guid)
        for score in scores:
            datetime = score['Score']['DateTime']
            filename = os.path.join(outdir, datetime.replace(':', '') + '.json')
            with open(filename, 'w') as f:
                json.dump(score, f)


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:p:")
    except getopt.GetoptError as err:
        print(err, file=sys.stderr)
        usage()
        sys.exit(1)

    smdir = os.path.expanduser('~/.stepmania-5.1')
    outdir = '.'
    player_guid = None

    for opt, arg in opts:
        if opt == '-i':
            smdir = arg
        elif opt == '-o':
            outdir = arg
        elif opt == '-p':
            player_guid = arg
        else:
            usage()

    convert_files(smdir, outdir, player_guid)


def usage():
    print('usage: convert.py [-i smdir] [-o outdir] [-p player-guid]', file=sys.stderr)
    sys.exit(1)


if __name__ == '__main__':
    main()
