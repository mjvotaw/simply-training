from calendar import monthrange
from datetime import date

from flask import Blueprint, jsonify

from sm_track.db import get_cursor
from sm_track.users.utils import require_logged_in


diary_blueprint = Blueprint('diary', __name__)


@diary_blueprint.route('/diary')
@require_logged_in
def diary(*, username):
    with get_cursor() as cur:
        cur.execute('''
            SELECT DISTINCT to_char(datetime::date, 'YYYY-MM') as date
            FROM score
            WHERE username = %s
            ORDER BY 1
        ''', (username,))
        rows = cur.fetchall()

    return jsonify({
        'months': [row['date'] for row in rows],
    })


@diary_blueprint.route('/diary/<int:year>-<int:month>')
@require_logged_in
def diary_month(*, username, year, month):
    _, days_in_month = monthrange(year, month)
    first_date = date(year, month, 1)
    last_date = date(year, month, days_in_month)

    with get_cursor() as cur:
        cur.execute('''
            SELECT
                datetime::date as date,
                sum(score.w1) as blue_fantastic,
                sum(case score.game_mode when 'FA+' then score.w2 else 0 end) as white_fantastic,
                sum(case score.game_mode when 'FA+' then score.w3 else score.w2 end) as excellent,
                sum(case score.game_mode when 'FA+' then score.w4 else score.w3 end) as great,
                sum(case score.game_mode when 'FA+' then score.w5 else score.w4 end) as decent,
                sum(case score.game_mode when 'FA+' then 0 else score.w5 end) as way_off,
                sum(score.miss) as miss,
                sum(score.survive_seconds) as survive_seconds,
                sum(w1 + w2 + w3 + w4 + w5) as steps,
                min(coalesce(custom_meter.meter, steps.meter, trail.meter))
                    filter (where score.grade <> 'Failed' AND score.music_rate >= 1.0) as min_meter,
                max(coalesce(custom_meter.meter, steps.meter, trail.meter))
                    filter (where score.grade <> 'Failed' AND score.music_rate >= 1.0) as max_meter,
                count(1) filter (where score.steps_hash is not null) as songs,
                count(1) filter (where score.trail_hash is not null) as courses
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN trail on trail.hash = score.trail_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                datetime::date BETWEEN %(first_date)s AND %(last_date)s
            GROUP BY datetime::date
            ORDER BY datetime::date
        ''', {
            'username': username,
            'first_date': first_date,
            'last_date': last_date,
        })
        rows = cur.fetchall()

    entries = []

    for row in rows:
        entries.append({
            'date': row['date'].isoformat(),
            'timing': {
                'blueFantastic': row['blue_fantastic'],
                'whiteFantastic': row['white_fantastic'],
                'excellent': row['excellent'],
                'great': row['great'],
                'decent': row['decent'],
                'wayOff': row['way_off'],
                'miss': row['miss'],
            },
            'surviveSeconds': row['survive_seconds'],
            'steps': row['steps'],
            'meter': {
                'min': row['min_meter'],
                'max': row['max_meter'],
            },
            'songs': row['songs'],
            'courses': row['courses'],
        })

    return jsonify({
        'entries': entries,
    })
