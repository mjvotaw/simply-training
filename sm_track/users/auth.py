from hashlib import pbkdf2_hmac
from secrets import compare_digest, token_bytes


def hash_password(password):
    iterations = 100_000
    salt = token_bytes(32)

    h = pbkdf2_hmac('sha256', password.encode(), salt, iterations)
    return '$'.join([str(iterations), salt.hex(), h.hex()])


def check_password(password, password_hash):
    iterations, salt, h = password_hash.split('$')

    iterations = int(iterations)
    salt = bytes.fromhex(salt)
    h = bytes.fromhex(h)

    h2 = pbkdf2_hmac('sha256', password.encode(), salt, iterations)
    return compare_digest(h, h2)
