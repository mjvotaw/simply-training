import calendar
import os
from base64 import b64decode
from datetime import date, timedelta

import jsonschema
from flask import Flask, jsonify, request, session
from flask_cors import CORS

from sm_track.db import get_cursor
from sm_track.diary.views import diary_blueprint
from sm_track.scores.views import scores_blueprint
from sm_track.users.views import users_blueprint
from sm_track.users.utils import require_logged_in


app = Flask(__name__)
app.secret_key = b64decode(os.getenv('SECRET_KEY'))
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024  # 1MB
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=7)
app.register_blueprint(diary_blueprint)
app.register_blueprint(scores_blueprint)
app.register_blueprint(users_blueprint)

CORS(app, origins=[
    'http://localhost:8080',
    'https://www.simply.training',
], supports_credentials=True)


def validate_search_params(data):
    jsonschema.validate(data, schema={
        'type': 'object',
        'properties': {
            'fromDate': {
                'type': 'string',
                'format': 'date',
            },
            'toDate': {
                'type': 'string',
                'format': 'date',
            },
            'stepsType': {
                'type': 'string',
                'enum': ['dance-single', 'dance-double', 'dance-douple'],
            },
            'difficulty': {
                'type': 'string',
                'enum': [
                    'Beginner', 'Easy', 'Medium', 'Hard', 'Challenge',
                    'Edit',
                ],
            },
            'minMeter': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 30,
            },
            'maxMeter': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 30,
            },
            'minBPM': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 999,
            },
            'maxBPM': {
                'type': 'integer',
                'minimum': 1,
                'maximum': 999,
            },
            'minGrade': {
                'type': 'string',
                'enum': [
                    'Tier01', 'Tier02', 'Tier03', 'Tier04', 'Tier05',
                    'Tier06', 'Tier07', 'Tier08', 'Tier09', 'Tier10',
                    'Tier11', 'Tier12', 'Tier13', 'Tier14', 'Tier15',
                    'Tier16', 'Tier17', 'Failed',
                ],
            },
            'maxGrade': {
                'type': 'string',
                'enum': [
                    'Tier01', 'Tier02', 'Tier03', 'Tier04', 'Tier05',
                    'Tier06', 'Tier07', 'Tier08', 'Tier09', 'Tier10',
                    'Tier11', 'Tier12', 'Tier13', 'Tier14', 'Tier15',
                    'Tier16', 'Tier17', 'Failed',
                ],
            },
            'minPercentDP': {
                'type': 'number',
                'minimum': 0,
                'maximum': 100,
            },
            'maxPercentDP': {
                'type': 'number',
                'minimum': 0,
                'maximum': 100,
            },
        },
        'additionalProperties': False,
    }, format_checker=jsonschema.draft7_format_checker)


@app.route('/stats/steps', methods=['POST'])
@require_logged_in
def step_stats(*, username):
    data = request.get_json()
    try:
        validate_search_params(data)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    with get_cursor() as cur:
        from_date = None
        to_date = None

        if 'fromDate' in data:
            from_date = date.fromisoformat(data['fromDate'])
        if 'toDate' in data:
            to_date = date.fromisoformat(data['toDate'])

        if not (from_date and to_date):
            cur.execute('''
            SELECT min(datetime::date), max(datetime::date)
            FROM score
            WHERE username = %s
            ''', (username,))
            first_score_date, last_score_date = cur.fetchone()

            if not from_date:
                from_date = first_score_date or date.today()
            if not to_date:
                to_date = last_score_date or date.today()

        span = to_date - from_date

        if span <= timedelta(days=112):   # 4 months
            resolution = 'day'
        elif span <= timedelta(days=730): # 2 years
            resolution = 'week'
        else:
            resolution = 'month'

        cur.execute('''
            SELECT
                date_trunc(%(resolution)s, datetime)::date as from_date,
                sum(w1 + w2 + w3 + w4 + w5) as steps
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN song ON song.hash = steps.song_hash
            LEFT JOIN trail on trail.hash = score.trail_hash
            -- LEFT JOIN course on course.hash = trail.course_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                datetime::date BETWEEN %(from_date)s AND %(to_date)s AND
                (%(difficulty)s IS NULL OR coalesce(steps.difficulty, trail.difficulty) = %(difficulty)s) AND
                (%(steps_type)s IS NULL OR coalesce(steps.steps_type, trail.steps_type) = %(steps_type)s) AND
                (%(min_meter)s IS NULL OR %(min_meter)s <= coalesce(custom_meter.meter, steps.meter, trail.meter)) AND
                (%(max_meter)s IS NULL OR coalesce(custom_meter.meter, steps.meter, trail.meter) <= %(max_meter)s) AND
                (%(min_bpm)s IS NULL OR %(min_bpm)s <= (song.high_bpm * score.music_rate)::integer) AND
                (%(max_bpm)s IS NULL OR (song.low_bpm * score.music_rate)::integer <= %(max_bpm)s) AND
                grade BETWEEN %(max_grade)s AND %(min_grade)s AND
                coalesce(%(min_percent_dp)s / 100. <= percent_dp, true) AND
                coalesce(percent_dp <= %(max_percent_dp)s / 100., true)
            GROUP BY 1
            ORDER BY 1
        ''', {
            'resolution': resolution,
            'username': username,
            'from_date': from_date,
            'to_date': to_date,
            'steps_type': data.get('stepsType'),
            'difficulty': data.get('difficulty'),
            'min_meter': data.get('minMeter'),
            'max_meter': data.get('maxMeter'),
            'min_bpm': data.get('minBPM'),
            'max_bpm': data.get('maxBPM'),
            'min_grade': data.get('minGrade', 'Failed'),
            'max_grade': data.get('maxGrade', 'Tier01'),
            'min_percent_dp': data.get('minPercentDP'),
            'max_percent_dp': data.get('maxPercentDP'),
        })
        rows = cur.fetchall()

    data = {
        'fromDate': from_date.isoformat(),
        'toDate': to_date.isoformat(),
        'resolution': resolution,
        'bars': [],
    }

    for row in rows:
        from_date = row['from_date']
        if resolution == 'day':
            to_date = from_date
            steps = row['steps']
        elif resolution == 'week':
            to_date = from_date + timedelta(days=6)
            steps = round(row['steps'] / 7)
        else:
            assert resolution == 'month'
            _, days = calendar.monthrange(from_date.year, from_date.month)
            to_date = date(from_date.year, from_date.month, days)
            steps = round(row['steps'] / days)

        bar = {
            'fromDate': from_date.isoformat(),
            'toDate': to_date.isoformat(),
            'steps': steps,
        }
        data['bars'].append(bar)

    return jsonify(data)


@app.route('/stats/timing', methods=['POST'])
@require_logged_in
def timing_stats(*, username):
    data = request.get_json()
    try:
        validate_search_params(data)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    with get_cursor() as cur:
        from_date = None
        to_date = None

        if 'fromDate' in data:
            from_date = date.fromisoformat(data['fromDate'])
        if 'toDate' in data:
            to_date = date.fromisoformat(data['toDate'])

        if not (from_date and to_date):
            cur.execute('''
            SELECT min(datetime::date), max(datetime::date)
            FROM score
            WHERE username = %s
            ''', (username,))
            first_score_date, last_score_date = cur.fetchone()

            if not from_date:
                from_date = first_score_date or date.today()
            if not to_date:
                to_date = last_score_date or date.today()

        span = to_date - from_date

        if span <= timedelta(days=112):   # 4 months
            resolution = 'day'
        elif span <= timedelta(days=730): # 2 years
            resolution = 'week'
        else:
            resolution = 'month'

        cur.execute('''
            SELECT
                date_trunc(%(resolution)s, datetime)::date as from_date,
                sum(w1) as blue_fantastic,
                sum(case game_mode when 'FA+' then w2 else 0 end) as white_fantastic,
                sum(case game_mode when 'FA+' then w3 else w2 end) as excellent,
                sum(case game_mode when 'FA+' then w4 else w3 end) as great,
                sum(case game_mode when 'FA+' then w5 else w4 end) as decent,
                sum(case game_mode when 'FA+' then 0 else w5 end) as way_off,
                sum(miss) as miss,
                sum(w1 + w2 + w3 + w4 + w5 + miss) as total
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN song ON song.hash = steps.song_hash
            LEFT JOIN trail on trail.hash = score.trail_hash
            -- LEFT JOIN course on course.hash = trail.course_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                datetime::date BETWEEN %(from_date)s AND %(to_date)s AND
                (%(difficulty)s IS NULL OR coalesce(steps.difficulty, trail.difficulty) = %(difficulty)s) AND
                (%(steps_type)s IS NULL OR coalesce(steps.steps_type, trail.steps_type) = %(steps_type)s) AND
                (%(min_meter)s IS NULL OR %(min_meter)s <= coalesce(custom_meter.meter, steps.meter, trail.meter)) AND
                (%(max_meter)s IS NULL OR coalesce(custom_meter.meter, steps.meter, trail.meter) <= %(max_meter)s) AND
                (%(min_bpm)s IS NULL OR %(min_bpm)s <= (song.high_bpm * score.music_rate)::integer) AND
                (%(max_bpm)s IS NULL OR (song.low_bpm * score.music_rate)::integer <= %(max_bpm)s) AND
                grade BETWEEN %(max_grade)s AND %(min_grade)s AND
                coalesce(%(min_percent_dp)s / 100. <= percent_dp, true) AND
                coalesce(percent_dp <= %(max_percent_dp)s / 100., true)
            GROUP BY 1
            ORDER BY 1
        ''', {
            'resolution': resolution,
            'username': username,
            'from_date': from_date,
            'to_date': to_date,
            'steps_type': data.get('stepsType'),
            'difficulty': data.get('difficulty'),
            'min_meter': data.get('minMeter'),
            'max_meter': data.get('maxMeter'),
            'min_bpm': data.get('minBPM'),
            'max_bpm': data.get('maxBPM'),
            'min_grade': data.get('minGrade', 'Failed'),
            'max_grade': data.get('maxGrade', 'Tier01'),
            'min_percent_dp': data.get('minPercentDP'),
            'max_percent_dp': data.get('maxPercentDP'),
        })
        rows = cur.fetchall()

    data = {
        'fromDate': from_date.isoformat(),
        'toDate': to_date.isoformat(),
        'resolution': resolution,
        'points': [],
    }

    for row in rows:
        from_date = row['from_date']
        if resolution == 'day':
            to_date = from_date
        elif resolution == 'week':
            to_date = from_date + timedelta(days=6)
        else:
            assert resolution == 'month'
            _, days = calendar.monthrange(from_date.year, from_date.month)
            to_date = date(from_date.year, from_date.month, days)

        point = {
            'fromDate': from_date.isoformat(),
            'toDate': to_date.isoformat(),
            'blueFantastic': row['blue_fantastic'] / row['total'],
            'whiteFantastic': row['white_fantastic'] / row['total'],
            'excellent': row['excellent'] / row['total'],
            'great': row['great'] / row['total'],
            'decent': row['decent'] / row['total'],
            'wayOff': row['way_off'] / row['total'],
            'miss': row['miss'] / row['total'],
        }
        data['points'].append(point)

    return jsonify(data)


@app.route('/stats/meter', methods=['POST'])
@require_logged_in
def meter_stats(*, username):
    data = request.get_json()
    try:
        validate_search_params(data)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    with get_cursor() as cur:
        from_date = None
        to_date = None

        if 'fromDate' in data:
            from_date = date.fromisoformat(data['fromDate'])
        if 'toDate' in data:
            to_date = date.fromisoformat(data['toDate'])

        if not (from_date and to_date):
            cur.execute('''
            SELECT min(datetime::date), max(datetime::date)
            FROM score
            WHERE username = %s
            ''', (username,))
            first_score_date, last_score_date = cur.fetchone()

            if not from_date:
                from_date = first_score_date or date.today()
            if not to_date:
                to_date = last_score_date or date.today()

        span = to_date - from_date

        if span <= timedelta(days=112):   # 4 months
            resolution = 'day'
        elif span <= timedelta(days=730): # 2 years
            resolution = 'week'
        else:
            resolution = 'month'

        cur.execute('''
            SELECT
                date_trunc(%(resolution)s, score.datetime)::date as from_date,
                min(coalesce(custom_meter.meter, steps.meter, trail.meter)) as min_meter,
                max(coalesce(custom_meter.meter, steps.meter, trail.meter)) as max_meter,
                percentile_disc(0.5) within group (
                    order by coalesce(custom_meter.meter, steps.meter, trail.meter)
                ) as median_meter
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN song ON song.hash = steps.song_hash
            LEFT JOIN trail on trail.hash = score.trail_hash
            -- LEFT JOIN course on course.hash = trail.course_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                datetime::date BETWEEN %(from_date)s AND %(to_date)s AND
                (%(difficulty)s IS NULL OR coalesce(steps.difficulty, trail.difficulty) = %(difficulty)s) AND
                (%(steps_type)s IS NULL OR coalesce(steps.steps_type, trail.steps_type) = %(steps_type)s) AND
                (%(min_meter)s IS NULL OR %(min_meter)s <= coalesce(custom_meter.meter, steps.meter, trail.meter)) AND
                (%(max_meter)s IS NULL OR coalesce(custom_meter.meter, steps.meter, trail.meter) <= %(max_meter)s) AND
                (%(min_bpm)s IS NULL OR %(min_bpm)s <= (song.high_bpm * score.music_rate)::integer) AND
                (%(max_bpm)s IS NULL OR (song.low_bpm * score.music_rate)::integer <= %(max_bpm)s) AND
                grade BETWEEN %(max_grade)s AND %(min_grade)s AND
                coalesce(%(min_percent_dp)s / 100. <= percent_dp, true) AND
                coalesce(percent_dp <= %(max_percent_dp)s / 100., true) AND
                score.music_rate >= 1.0
            GROUP BY 1
            ORDER BY 1
        ''', {
            'resolution': resolution,
            'username': username,
            'from_date': from_date,
            'to_date': to_date,
            'steps_type': data.get('stepsType'),
            'difficulty': data.get('difficulty'),
            'min_meter': data.get('minMeter'),
            'max_meter': data.get('maxMeter'),
            'min_bpm': data.get('minBPM'),
            'max_bpm': data.get('maxBPM'),
            'min_grade': data.get('minGrade', 'Failed'),
            'max_grade': data.get('maxGrade', 'Tier01'),
            'min_percent_dp': data.get('minPercentDP'),
            'max_percent_dp': data.get('maxPercentDP'),
        })
        rows = cur.fetchall()

    data = {
        'fromDate': from_date.isoformat(),
        'toDate': to_date.isoformat(),
        'resolution': resolution,
        'points': [],
    }

    for row in rows:
        from_date = row['from_date']
        if resolution == 'day':
            to_date = from_date
        elif resolution == 'week':
            to_date = from_date + timedelta(days=6)
        else:
            assert resolution == 'month'
            _, days = calendar.monthrange(from_date.year, from_date.month)
            to_date = date(from_date.year, from_date.month, days)

        point = {
            'fromDate': from_date.isoformat(),
            'toDate': to_date.isoformat(),
            'minMeter': row['min_meter'],
            'maxMeter': row['max_meter'],
            'medianMeter': row['median_meter'],
            'difference': row['max_meter'] - row['min_meter'],
        }
        data['points'].append(point)

    return jsonify(data)


@app.route('/re-rate', methods=['POST'])
@require_logged_in
def rerate(*, username):
    if session['demo']:
        return jsonify(error='Re-rating charts is disabled for the demo account!'), 403

    data = request.get_json()
    try:
        jsonschema.validate(data, schema={
            'properties': {
                'steps': {
                    'type': 'string',
                },
                'trail': {
                    'type': 'string',
                },
                'meter': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 30,
                },
            },
            'oneOf': [
                {
                    'required': ['steps', 'meter'],
                },
                {
                    'required': ['trail', 'meter'],
                },
            ],
            'additionalProperties': False,
        }, format_checker=jsonschema.draft7_format_checker)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    with get_cursor() as cur:
        if 'steps' in data:
            cur.execute('''
                SELECT meter FROM steps WHERE hash = %s
            ''', (data['steps'],))
            original_meter, = cur.fetchone()

            if data['meter'] == original_meter:
                cur.execute('''
                    DELETE FROM custom_meter
                    WHERE username = %s AND steps_hash = %s
                ''', (username, data['steps']))
            else:
                cur.execute('''
                    INSERT INTO custom_meter (username, steps_hash, meter)
                    VALUES (%(username)s, %(steps)s, %(meter)s)
                    ON CONFLICT (username, steps_hash)
                        DO UPDATE SET meter = %(meter)s
                ''', {
                    'username': username,
                    'steps': data['steps'],
                    'meter': data['meter'],
                })
        else:
            cur.execute('''
                SELECT meter FROM trail WHERE hash = %s
            ''', (data['trail'],))
            original_meter, = cur.fetchone()

            if data['meter'] == original_meter:
                cur.execute('''
                    DELETE FROM custom_meter
                    WHERE username = %s AND trail_hash = %s
                ''', (username, data['trail']))
            else:
                cur.execute('''
                    INSERT INTO custom_meter (username, trail_hash, meter)
                    VALUES (%(username)s, %(trail)s, %(meter)s)
                    ON CONFLICT (username, trail_hash)
                        DO UPDATE SET meter = %(meter)s
                ''', {
                    'username': username,
                    'trail': data['trail'],
                    'meter': data['meter'],
                })

    return '', 204
