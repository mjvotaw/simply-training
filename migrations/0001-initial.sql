CREATE TYPE grade AS ENUM (
    'Tier01',
    'Tier02',
    'Tier03',
    'Tier04',
    'Tier05',
    'Tier06',
    'Tier07',
    'Tier08',
    'Tier09',
    'Tier10',
    'Tier11',
    'Tier12',
    'Tier13',
    'Tier14',
    'Tier15',
    'Tier16',
    'Tier17',
    'Failed'
);
CREATE TYPE steps_type AS ENUM (
    'dance-single',
    'dance-double',
    'dance-couple'
);
CREATE TYPE steps_difficulty AS ENUM (
    'Beginner',
    'Easy',
    'Medium',
    'Hard',
    'Challenge',
    'Edit'
);

CREATE TYPE game_mode AS ENUM (
    'Casual',
    'ITG',
    'FA+'
);

CREATE TABLE account (
    name TEXT PRIMARY KEY,
    password_hash TEXT NOT NULL,
    email TEXT NOT NULL,
    translit BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE song (
    hash CHAR(16) PRIMARY KEY,

    dir TEXT NOT NULL,
    pack TEXT NOT NULL,
    title TEXT NOT NULL,
    subtitle TEXT,
    artist TEXT,
    translit_title TEXT NOT NULL,
    translit_subtitle TEXT,
    translit_artist TEXT,
    genre TEXT,
    low_bpm INTEGER NOT NULL CHECK (low_bpm > 0),
    high_bpm INTEGER NOT NULL CHECK (high_bpm >= low_bpm),
    random_bpm BOOLEAN NOT NULL,
    music_length_seconds INTEGER NOT NULL CHECK (music_length_seconds > 0)
);
CREATE INDEX ON song (low_bpm);
CREATE INDEX ON song (high_bpm);

CREATE TABLE steps (
    hash CHAR(16) PRIMARY KEY,

    song_hash CHAR(16) NOT NULL REFERENCES song (hash),
    difficulty steps_difficulty NOT NULL,
    meter INTEGER NOT NULL CHECK (meter > 0),
    steps_type steps_type NOT NULL,
    author_credit TEXT NOT NULL,
    description TEXT NOT NULL,

    radar_notes INTEGER NOT NULL CHECK (radar_notes >= 0),
    radar_taps_and_holds INTEGER NOT NULL CHECK (radar_taps_and_holds >= 0),
    radar_jumps INTEGER NOT NULL CHECK (radar_jumps >= 0),
    radar_holds INTEGER NOT NULL CHECK (radar_holds >= 0),
    radar_mines INTEGER NOT NULL CHECK (radar_mines >= 0),
    radar_hands INTEGER NOT NULL CHECK (radar_hands >= 0),
    radar_rolls INTEGER NOT NULL CHECK (radar_rolls >= 0)
);
CREATE INDEX ON steps (difficulty);
CREATE INDEX ON steps (steps_type);
CREATE INDEX ON steps (meter);

CREATE TABLE course (
    hash CHAR(16) PRIMARY KEY,

    path TEXT,
    full_title TEXT NOT NULL,
    translit_full_title TEXT NOT NULL,
    scripter TEXT,
    description TEXT
);

CREATE TABLE trail (
    hash CHAR(16) PRIMARY KEY,

    course_hash CHAR(16) NOT NULL REFERENCES course (hash),
    difficulty steps_difficulty NOT NULL,
    meter INTEGER NOT NULL CHECK (meter > 0),
    steps_type steps_type NOT NULL,
    length_seconds INTEGER NOT NULL CHECK (length_seconds > 0),

    radar_notes INTEGER NOT NULL CHECK (radar_notes >= 0),
    radar_taps_and_holds INTEGER NOT NULL CHECK (radar_taps_and_holds >= 0),
    radar_jumps INTEGER NOT NULL CHECK (radar_jumps >= 0),
    radar_holds INTEGER NOT NULL CHECK (radar_holds >= 0),
    radar_mines INTEGER NOT NULL CHECK (radar_mines >= 0),
    radar_hands INTEGER NOT NULL CHECK (radar_hands >= 0),
    radar_rolls INTEGER NOT NULL CHECK (radar_rolls >= 0)
);
CREATE INDEX ON trail (difficulty);
CREATE INDEX ON trail (steps_type);
CREATE INDEX ON trail (meter);

CREATE TABLE score (
    guid TEXT PRIMARY KEY,

    username TEXT NOT NULL REFERENCES account (name),
    steps_hash CHAR(16) REFERENCES steps (hash),
    trail_hash CHAR(16) REFERENCES trail (hash),

    game_mode game_mode,
    machine_guid TEXT NOT NULL,

    grade grade NOT NULL,
    score INTEGER NOT NULL,
    percent_dp FLOAT NOT NULL,
    survive_seconds INTEGER NOT NULL,
    max_combo INTEGER NOT NULL,
    modifiers TEXT NOT NULL,
    datetime TIMESTAMP NOT NULL,
    player_guid TEXT,
    disqualified BOOLEAN NOT NULL,

    -- tap scores
    w1 INTEGER NOT NULL,
    w2 INTEGER NOT NULL,
    w3 INTEGER NOT NULL,
    w4 INTEGER NOT NULL,
    w5 INTEGER NOT NULL,
    miss INTEGER NOT NULL,
    hit_mine INTEGER NOT NULL,
    avoid_mine INTEGER NOT NULL,
    checkpoint_miss INTEGER NOT NULL,
    checkpoint_hit INTEGER NOT NULL,

    -- hold scores
    let_go INTEGER NOT NULL,
    held INTEGER NOT NULL,
    missed_hold INTEGER NOT NULL,

    radar_notes INTEGER NOT NULL CHECK (radar_notes >= 0),
    radar_taps_and_holds INTEGER NOT NULL CHECK (radar_taps_and_holds >= 0),
    radar_jumps INTEGER NOT NULL CHECK (radar_jumps >= 0),
    radar_holds INTEGER NOT NULL CHECK (radar_holds >= 0),
    radar_mines INTEGER NOT NULL CHECK (radar_mines >= 0),
    radar_hands INTEGER NOT NULL CHECK (radar_hands >= 0),
    radar_rolls INTEGER NOT NULL CHECK (radar_rolls >= 0),

    CHECK ((steps_hash IS NULL) != (trail_hash IS NULL))
);
CREATE INDEX ON score (username);
CREATE INDEX ON score (datetime);
CREATE INDEX ON score (grade);
CREATE INDEX ON score (percent_dp);
