from contextlib import contextmanager
from threading import Lock

from psycopg2.extras import DictCursor
from psycopg2.pool import ThreadedConnectionPool


_pool = None
_pool_lock = Lock()


@contextmanager
def get_cursor():
    global _pool
    with _pool_lock:
        if _pool is None:
            _pool = ThreadedConnectionPool(1, 20, dbname='sm-track', cursor_factory=DictCursor)

    conn = _pool.getconn()
    try:
        with conn:
            with conn.cursor() as cur:
                yield cur
    finally:
        _pool.putconn(conn)
