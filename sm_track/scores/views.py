import json
import re
from datetime import date
from hashlib import shake_128

import jsonschema
from flask import Blueprint, jsonify, request, session
from psycopg2.sql import SQL

from sm_track.db import get_cursor
from sm_track.users.utils import require_logged_in


scores_blueprint = Blueprint('scores', __name__)


@scores_blueprint.route('/first-clears')
@require_logged_in
def first_clears(*, username):
    with get_cursor() as cur:
        cur.execute('''
            SELECT DISTINCT ON (
                coalesce(steps.steps_type, trail.steps_type),
                coalesce(custom_meter.meter, steps.meter, trail.meter)
            )
                score.guid,
                score.steps_hash,
                score.trail_hash,
                coalesce(steps.steps_type, trail.steps_type) as steps_type,
                coalesce(steps.difficulty, trail.difficulty) as difficulty,
                coalesce(custom_meter.meter, steps.meter, trail.meter) as meter,
                score.datetime,
                score.percent_dp,
                score.grade,
                song.pack as song_pack,
                song.title as song_title,
                song.translit_title as song_translit_title,
                song.subtitle as song_subtitle,
                song.translit_subtitle as song_translit_subtitle,
                course.full_title as course_full_title,
                course.translit_full_title as course_translit_full_title
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN song ON song.hash = steps.song_hash
            LEFT JOIN trail ON trail.hash = score.trail_hash
            LEFT JOIN course ON course.hash = trail.course_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                score.grade <> 'Failed' AND
                score.music_rate >= 1.0 AND
                coalesce(custom_meter.meter, steps.meter, trail.meter) <= 30
            ORDER BY
                coalesce(steps.steps_type, trail.steps_type),
                coalesce(custom_meter.meter, steps.meter, trail.meter),
                score.datetime
        ''', {
            'username': username,
        })
        rows = cur.fetchall()

    first_clears = []

    for row in rows:
        first_clears.append({
            'guid': row['guid'],
            'stepsType': row['steps_type'],
            'difficulty': row['difficulty'],
            'meter': row['meter'],
            'datetime': row['datetime'].isoformat(),
            'percentDP': row['percent_dp'],
            'grade': row['grade'],
            'song': {
                'pack': row['song_pack'],
                'title': row['song_title'],
                'subtitle': row['song_subtitle'],
                'translit_title': row['song_translit_title'],
                'translit_subtitle': row['song_translit_subtitle'],
            } if row['steps_hash'] else None,
            'course': {
                'full_title': row['course_full_title'],
                'translit_full_title': row['course_translit_full_title'],
            } if row['trail_hash'] else None,
        })

    return jsonify({
        'firstClears': first_clears,
    })


@scores_blueprint.route('/scores', methods=['POST'])
@require_logged_in
def search(*, username):
    data = request.get_json()
    try:
        jsonschema.validate(data, schema={
            'type': 'object',
            'properties': {
                'query': {
                    'type': 'string',
                },
                'fromDate': {
                    'type': 'string',
                    'format': 'date',
                },
                'toDate': {
                    'type': 'string',
                    'format': 'date',
                },
                'stepsType': {
                    'type': 'string',
                    'enum': ['dance-single', 'dance-double', 'dance-douple'],
                },
                'difficulty': {
                    'type': 'string',
                    'enum': [
                        'Beginner', 'Easy', 'Medium', 'Hard', 'Challenge',
                        'Edit',
                    ],
                },
                'minMeter': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 30,
                },
                'maxMeter': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 30,
                },
                'minBPM': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 999,
                },
                'maxBPM': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 999,
                },
                'minGrade': {
                    'type': 'string',
                    'enum': [
                        'Tier01', 'Tier02', 'Tier03', 'Tier04', 'Tier05',
                        'Tier06', 'Tier07', 'Tier08', 'Tier09', 'Tier10',
                        'Tier11', 'Tier12', 'Tier13', 'Tier14', 'Tier15',
                        'Tier16', 'Tier17', 'Failed',
                    ],
                },
                'maxGrade': {
                    'type': 'string',
                    'enum': [
                        'Tier01', 'Tier02', 'Tier03', 'Tier04', 'Tier05',
                        'Tier06', 'Tier07', 'Tier08', 'Tier09', 'Tier10',
                        'Tier11', 'Tier12', 'Tier13', 'Tier14', 'Tier15',
                        'Tier16', 'Tier17', 'Failed',
                    ],
                },
                'minPercentDP': {
                    'type': 'number',
                    'minimum': 0,
                    'maximum': 100,
                },
                'maxPercentDP': {
                    'type': 'number',
                    'minimum': 0,
                    'maximum': 100,
                },
                'paginate': {
                    'type': 'boolean',
                },
                'pageStartDatetime': {
                    'type': 'string',
                    'format': 'datetime',
                },
                'pageStartScoreGuid': {
                    'type': 'string',
                },
                'reverse': {
                    'type': 'boolean',
                },
            },
            'additionalProperties': False,
        }, format_checker=jsonschema.draft7_format_checker)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    paginate = data.get('paginate', False)
    reverse = data.get('reverse', False)

    with get_cursor() as cur:
        cur.execute(SQL('''
            SELECT
                score.guid,
                score.steps_hash, score.trail_hash,
                coalesce(steps.steps_type, trail.steps_type) as steps_type,
                coalesce(steps.difficulty, trail.difficulty) as difficulty,
                score.game_mode, score.datetime, score.percent_dp, score.grade,
                score.max_combo, score.modifiers, score.music_rate,
                score.w1 as blue_fantastic,
                case score.game_mode when 'FA+' then score.w2 else 0 end as white_fantastic,
                case score.game_mode when 'FA+' then score.w3 else score.w2 end as excellent,
                case score.game_mode when 'FA+' then score.w4 else score.w3 end as great,
                case score.game_mode when 'FA+' then score.w5 else score.w4 end as decent,
                case score.game_mode when 'FA+' then 0 else score.w5 end as way_off,
                miss, hit_mine, avoid_mine, let_go, held,
                missed_hold,
                score.radar_notes as actual_radar_notes,
                score.radar_taps_and_holds as actual_radar_taps_and_holds,
                score.radar_jumps as actual_radar_jumps,
                score.radar_holds as actual_radar_holds,
                score.radar_mines as actual_radar_mines,
                score.radar_hands as actual_radar_hands,
                score.radar_rolls as actual_radar_rolls,
                coalesce(steps.radar_notes, trail.radar_notes) as possible_radar_notes,
                coalesce(steps.radar_taps_and_holds, trail.radar_taps_and_holds) as possible_radar_taps_and_holds,
                coalesce(steps.radar_jumps, trail.radar_jumps) as possible_radar_jumps,
                coalesce(steps.radar_holds, trail.radar_holds) as possible_radar_holds,
                coalesce(steps.radar_mines, trail.radar_mines) as possible_radar_mines,
                coalesce(steps.radar_hands, trail.radar_hands) as possible_radar_hands,
                coalesce(steps.radar_rolls, trail.radar_rolls) as possible_radar_rolls,
                song.pack as song_pack,
                song.title as song_title,
                song.translit_title as song_translit_title,
                song.subtitle as song_subtitle,
                song.translit_subtitle as song_translit_subtitle,
                song.low_bpm, song.high_bpm, song.random_bpm,
                steps.author_credit as steps_author_credit,
                steps.description as steps_description,
                course.full_title as course_full_title,
                course.translit_full_title as course_translit_full_title,
                course.scripter as course_scripter,
                course.description as course_description,
                coalesce(custom_meter.meter, steps.meter, trail.meter) as meter,
                coalesce(steps.meter, trail.meter) as original_meter
            FROM score
            LEFT JOIN steps ON steps.hash = score.steps_hash
            LEFT JOIN song ON song.hash = steps.song_hash
            LEFT JOIN trail on trail.hash = score.trail_hash
            LEFT JOIN course on course.hash = trail.course_hash
            LEFT JOIN custom_meter
                ON
                    custom_meter.username = %(username)s AND
                    (custom_meter.steps_hash = score.steps_hash OR custom_meter.trail_hash = score.trail_hash)
            WHERE
                score.username = %(username)s AND
                (
                    %(query)s IS NULL OR
                    song.search_vector @@ plainto_tsquery('simple', %(query)s) OR
                    steps.search_vector @@ plainto_tsquery('simple', %(query)s) OR
                    course.search_vector @@ plainto_tsquery('simple', %(query)s)
                ) AND
                (%(from_date)s IS NULL OR datetime::date >= %(from_date)s) AND
                (%(to_date)s IS NULL OR datetime::date <= %(to_date)s) AND
                (%(difficulty)s IS NULL OR coalesce(steps.difficulty, trail.difficulty) = %(difficulty)s) AND
                (%(steps_type)s IS NULL OR coalesce(steps.steps_type, trail.steps_type) = %(steps_type)s) AND
                (%(min_meter)s IS NULL OR %(min_meter)s <= coalesce(custom_meter.meter, steps.meter, trail.meter)) AND
                (%(max_meter)s IS NULL OR coalesce(custom_meter.meter, steps.meter, trail.meter) <= %(max_meter)s) AND
                (%(min_bpm)s IS NULL OR %(min_bpm)s <= (song.high_bpm * score.music_rate)::integer) AND
                (%(max_bpm)s IS NULL OR (song.low_bpm * score.music_rate)::integer <= %(max_bpm)s) AND
                grade BETWEEN %(max_grade)s AND %(min_grade)s AND
                coalesce(%(min_percent_dp)s / 100. <= percent_dp, true) AND
                coalesce(percent_dp <= %(max_percent_dp)s / 100., true) AND
                (
                    %(page_start_datetime)s IS NULL OR
                    %(page_start_score_guid)s IS NULL OR
                    (datetime, guid) >= (%(page_start_datetime)s, %(page_start_score_guid)s)
                )
            ORDER BY datetime {sort_direction}, guid
            {limit}
        ''').format(
            sort_direction=SQL('DESC') if reverse else SQL('ASC'),
            limit=SQL('LIMIT 101') if paginate else SQL(''),
        ), {
            'username': username,
            'query': data.get('query'),
            'from_date': date.fromisoformat(data['fromDate']) if 'fromDate' in data else None,
            'to_date': date.fromisoformat(data['toDate']) if 'toDate' in data else None,
            'steps_type': data.get('stepsType'),
            'difficulty': data.get('difficulty'),
            'min_meter': data.get('minMeter'),
            'max_meter': data.get('maxMeter'),
            'min_bpm': data.get('minBPM'),
            'max_bpm': data.get('maxBPM'),
            'min_grade': data.get('minGrade', 'Failed'),
            'max_grade': data.get('maxGrade', 'Tier01'),
            'min_percent_dp': data.get('minPercentDP'),
            'max_percent_dp': data.get('maxPercentDP'),
            'page_start_datetime': data.get('pageStartDatetime'),
            'page_start_score_guid': data.get('pageStartScoreGuid'),
        })
        rows = cur.fetchall()

    scores = []

    for row in rows:
        scores.append({
            'guid': row['guid'],
            'datetime': row['datetime'].isoformat(),
            'song': {
                'pack': row['song_pack'],
                'title': row['song_title'],
                'subtitle': row['song_subtitle'],
                'translit_title': row['song_translit_title'],
                'translit_subtitle': row['song_translit_subtitle'],
            } if row['steps_hash'] else None,
            'steps': {
                'author_credit': row['steps_author_credit'],
                'description': row['steps_description'],
                'hash': row['steps_hash'],
            } if row['steps_hash'] else None,
            'course': {
                'full_title': row['course_full_title'],
                'translit_full_title': row['course_translit_full_title'],
                'scripter': row['course_scripter'],
                'description': row['course_description'],
            } if row['trail_hash'] else None,
            'trail': {
                'hash': row['trail_hash'],
            } if row['trail_hash'] else None,
            'stepsType': row['steps_type'],
            'difficulty': row['difficulty'],
            'meter': row['meter'],
            'originalMeter': row['original_meter'],
            'bpm': {
                'low': row['low_bpm'],
                'high': row['high_bpm'],
                'random': row['random_bpm'],
            },
            'percentDP': row['percent_dp'],
            'grade': row['grade'],
            'maxCombo': row['max_combo'],
            'modifiers': row['modifiers'],
            'musicRate': row['music_rate'],
            'tapNoteScores': {
                'blueFantastic': row['blue_fantastic'],
                'whiteFantastic': row['white_fantastic'],
                'excellent': row['excellent'],
                'great': row['great'],
                'decent': row['decent'],
                'wayOff': row['way_off'],
                'miss': row['miss'],
                'hitMine': row['hit_mine'],
                'avoidMine': row['avoid_mine'],
            },
            'holdNoteScores': {
                'held': row['held'],
                'letGo': row['let_go'],
                'missed': row['missed_hold'],
            },
            'gameMode': row['game_mode'],
            'radarPossible': {
                'notes': row['possible_radar_notes'],
                'tapsAndHolds': row['possible_radar_taps_and_holds'],
                'jumps': row['possible_radar_jumps'],
                'holds': row['possible_radar_holds'],
                'mines': row['possible_radar_mines'],
                'hands': row['possible_radar_hands'],
                'rolls': row['possible_radar_rolls'],
            },
            'radarActual': {
                'notes': row['actual_radar_notes'],
                'tapsAndHolds': row['actual_radar_taps_and_holds'],
                'jumps': row['actual_radar_jumps'],
                'holds': row['actual_radar_holds'],
                'mines': row['actual_radar_mines'],
                'hands': row['actual_radar_hands'],
                'rolls': row['actual_radar_rolls'],
            },
        })

    next_page = None
    if paginate and len(scores) > 100:
        next_score = scores.pop(-1)
        next_page = {
            'pageStartDatetime': next_score['datetime'],
            'pageStartScoreGuid': next_score['guid'],
        }

    return jsonify({
        'scores': scores,
        'nextPage': next_page,
    })


@scores_blueprint.route('/upload', methods=['POST'])
@require_logged_in
def upload(*, username):
    if session['demo']:
        return jsonify(error='Uploads are disabled for the demo account!'), 403

    data = request.get_json()
    try:
        jsonschema.validate(data, schema={
            'type': 'object',
            'properties': {
                'Version': {
                    'type': 'integer',
                    'minimum': 1,
                    'maximum': 30,
                },
                'Theme': {
                    'type': 'string',
                },
                'ThemeVersion': {
                    'type': 'string',
                },
                'ProductID': {
                    'type': 'string',
                },
                'ProductVersion': {
                    'type': 'string',
                },
                'MachineGuid': {
                    'type': 'string',
                },
                'GameMode': {
                    'type': 'string',
                    'enum': ['Casual', 'ITG', 'FA+'],
                },
                'Song': {
                    'type': 'object',
                    'properties': {
                        'Dir': {
                            'type': 'string',
                        },
                        'Group': {
                            'type': 'string',
                        },
                        'Title': {
                            'type': 'string',
                        },
                        'SubTitle': {
                            'type': 'string',
                        },
                        'Artist': {
                            'type': 'string',
                        },
                        'TranslitTitle': {
                            'type': 'string',
                        },
                        'TranslitSubTitle': {
                            'type': 'string',
                        },
                        'TranslitArtist': {
                            'type': 'string',
                        },
                        'Genre': {
                            'type': 'string',
                        },
                        'BPM': {
                            'type': 'array',
                            'items': {
                                'type': 'number',
                            },
                            'minItems': 2,
                            'maxItems': 2,
                        },
                        'RandomBPM': {
                            'type': 'boolean',
                        },
                        'MusicLengthSeconds': {
                            'type': 'number',
                            'minimum': 0,
                        },
                    },
                    'required': [
                        'Dir', 'Group', 'Title', 'TranslitTitle', 'BPM',
                        'RandomBPM', 'MusicLengthSeconds',
                    ],
                    'additionalProperties': False,
                },
                'Steps': {
                    'type': 'object',
                    'properties': {
                        'Difficulty': {
                            'type': 'string',
                            'enum': [
                                'Beginner', 'Easy', 'Medium', 'Hard',
                                'Challenge', 'Edit',
                            ],
                        },
                        'Meter': {
                            'type': 'integer',
                            'minimum': 1,
                        },
                        'StepsType': {
                            'type': 'string',
                            'enum': ['dance-single', 'dance-double', 'dance-douple'],
                        },
                        'AuthorCredit': {
                            'type': 'string',
                        },
                        'Description': {
                            'type': 'string',
                        },
                        'Radar': {
                            'type': 'object',
                            'properties': {
                                'Notes': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'TapsAndHolds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Jumps': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Holds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Mines': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Hands': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Rolls': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                            },
                            'required': [
                                'Notes', 'TapsAndHolds', 'Jumps', 'Holds',
                                'Mines', 'Hands', 'Rolls',
                            ],
                            'additionalProperties': False,
                        },
                    },
                    'required': [
                        'Difficulty', 'Meter', 'StepsType', 'AuthorCredit',
                        'Description', 'Radar',
                    ],
                    'additionalProperties': False,
                },
                'Course': {
                    'type': 'object',
                    'properties': {
                        'Path': {
                            'type': 'string',
                        },
                        'FullTitle': {
                            'type': 'string',
                        },
                        'TranslitFullTitle': {
                            'type': 'string',
                        },
                        'Scripter': {
                            'type': 'string',
                        },
                        'Description': {
                            'type': 'string',
                        },
                    },
                    'required': [
                        'FullTitle', 'TranslitFullTitle',
                    ],
                    'additionalProperties': False,
                },
                'Trail': {
                    'type': 'object',
                    'properties': {
                        'Difficulty': {
                            'type': 'string',
                            'enum': [
                                'Beginner', 'Easy', 'Medium', 'Hard',
                                'Challenge', 'Edit',
                            ],
                        },
                        'Meter': {
                            'type': 'integer',
                            'minimum': 1,
                        },
                        'StepsType': {
                            'type': 'string',
                            'enum': ['dance-single', 'dance-double', 'dance-douple'],
                        },
                        'LengthSeconds': {
                            'type': 'number',
                            'minimum': 0,
                        },
                        'Radar': {
                            'type': 'object',
                            'properties': {
                                'Notes': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'TapsAndHolds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Jumps': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Holds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Mines': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Hands': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Rolls': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                            },
                            'required': [
                                'Notes', 'TapsAndHolds', 'Jumps', 'Holds',
                                'Mines', 'Hands', 'Rolls',
                            ],
                            'additionalProperties': False,
                        },
                    },
                    'required': [
                        'Difficulty', 'Meter', 'StepsType', 'LengthSeconds',
                    ],
                    'additionalProperties': False,
                },
                'Score': {
                    'type': 'object',
                    'properties': {
                        'Guid': {
                            'type': 'string',
                        },
                        'Grade': {
                            'type': 'string',
                        },
                        'Score': {
                            'type': 'integer',
                            'minimum': 0,
                        },
                        'PercentDP': {
                            'type': 'number',
                            'minimum': 0,
                            'maximum': 1,
                        },
                        'SurviveSeconds': {
                            'type': 'number',
                            'minimum': 0,
                        },
                        'MaxCombo': {
                            'type': 'integer',
                            'minimum': 0,
                        },
                        'Modifiers': {
                            'type': 'string',
                        },
                        'DateTime': {
                            'type': 'string',
                            'format': 'datetime',
                        },
                        'PlayerGuid': {
                            'type': 'string',
                        },
                        'Disqualified': {
                            'type': 'boolean',
                        },
                        'TapNoteScores': {
                            'type': 'object',
                            'properties': {
                                'W1': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'W2': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'W3': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'W4': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'W5': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Miss': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'HitMine': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'AvoidMine': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'CheckpointMiss': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'CheckpointHit': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                            },
                            'required': [
                                'W1', 'W2', 'W3', 'W4', 'W5', 'Miss',
                                'HitMine', 'AvoidMine', 'CheckpointMiss',
                                'CheckpointHit',
                            ],
                            'additionalProperties': False,
                        },
                        'HoldNoteScores': {
                            'type': 'object',
                            'properties': {
                                'LetGo': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Held': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'MissedHold': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                            },
                            'required': [
                                'LetGo', 'Held', 'MissedHold',
                            ],
                            'additionalProperties': False,
                        },
                        'Radar': {
                            'type': 'object',
                            'properties': {
                                'Notes': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'TapsAndHolds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Jumps': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Holds': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Mines': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Hands': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                                'Rolls': {
                                    'type': 'integer',
                                    'minimum': 0,
                                },
                            },
                            'required': [
                                'Notes', 'TapsAndHolds', 'Jumps', 'Holds',
                                'Mines', 'Hands', 'Rolls',
                            ],
                            'additionalProperties': False,
                        },
                    },
                    'required': [
                        'Guid', 'Grade', 'Score', 'PercentDP',
                        'SurviveSeconds', 'MaxCombo', 'Modifiers', 'DateTime',
                        'PlayerGuid', 'Disqualified', 'TapNoteScores',
                        'HoldNoteScores', 'Radar',
                    ],
                    'additionalProperties': False,
                },
            },
            'required': [
                'Version', 'Theme', 'ThemeVersion', 'ProductID',
                'ProductVersion', 'MachineGuid', 'GameMode', 'Score',
            ],
            'additionalProperties': False,
        }, format_checker=jsonschema.draft7_format_checker)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    steps_hash = None
    trail_hash = None

    with get_cursor() as cur:
        if 'Song' in data:
            song = data['Song']
            steps = data['Steps']

            song_hash = shake_128(json.dumps(song, sort_keys=True).encode()).hexdigest(8)

            cur.execute('''
                INSERT INTO song (
                    hash, dir, pack, title, subtitle, artist, translit_title,
                    translit_subtitle, translit_artist, genre, low_bpm,
                    high_bpm, random_bpm, music_length_seconds
                ) VALUES (
                    %(hash)s, %(dir)s, %(pack)s, %(title)s, %(subtitle)s,
                    %(artist)s, %(translit_title)s, %(translit_subtitle)s,
                    %(translit_artist)s, %(genre)s, %(low_bpm)s, %(high_bpm)s,
                    %(random_bpm)s, %(music_length_seconds)s
                ) ON CONFLICT (hash) DO NOTHING
            ''', {
                'hash': song_hash,
                'dir': song['Dir'],
                'pack': song['Group'],
                'title': song['Title'],
                'subtitle': song.get('SubTitle'),
                'artist': song.get('Artist'),
                'translit_title': song['TranslitTitle'],
                'translit_subtitle': song.get('TranslitSubTitle'),
                'translit_artist': song.get('TranslitArtist'),
                'genre': song.get('Genre'),
                'low_bpm': round(song['BPM'][0]),
                'high_bpm': round(song['BPM'][1]),
                'random_bpm': song['RandomBPM'],
                'music_length_seconds': round(song['MusicLengthSeconds']),
            })

            steps['song_hash'] = song_hash
            steps_hash = shake_128(json.dumps(steps, sort_keys=True).encode()).hexdigest(8)

            radar = steps['Radar']

            cur.execute('''
                INSERT INTO steps (
                    hash, song_hash, difficulty, meter, steps_type,
                    author_credit, description, radar_notes,
                    radar_taps_and_holds, radar_jumps, radar_holds,
                    radar_mines, radar_hands, radar_rolls
                ) VALUES (
                    %(hash)s, %(song_hash)s, %(difficulty)s, %(meter)s,
                    %(steps_type)s, %(author_credit)s, %(description)s,
                    %(radar_notes)s, %(radar_taps_and_holds)s, %(radar_jumps)s,
                    %(radar_holds)s, %(radar_mines)s, %(radar_hands)s,
                    %(radar_rolls)s
                ) ON CONFLICT (hash) DO NOTHING
            ''', {
                'hash': steps_hash,
                'song_hash': song_hash,
                'difficulty': steps['Difficulty'],
                'meter': steps['Meter'],
                'steps_type': steps['StepsType'],
                'author_credit': steps['AuthorCredit'],
                'description': steps['Description'],
                'radar_notes': radar['Notes'],
                'radar_taps_and_holds': radar['TapsAndHolds'],
                'radar_jumps': radar['Jumps'],
                'radar_holds': radar['Holds'],
                'radar_mines': radar['Mines'],
                'radar_hands': radar['Hands'],
                'radar_rolls': radar['Rolls'],
            })

        if 'Course' in data:
            course = data['Course']
            trail = data['Trail']

            course_hash = shake_128(json.dumps(course, sort_keys=True).encode()).hexdigest(8)

            cur.execute('''
                INSERT INTO course (
                    hash, path, full_title, translit_full_title, scripter,
                    description
                ) VALUES (
                    %(hash)s, %(path)s, %(full_title)s,
                    %(translit_full_title)s, %(scripter)s, %(description)s
                ) ON CONFLICT (hash) DO NOTHING
            ''', {
                'hash': course_hash,
                'path': course.get('Path'),
                'full_title': course['FullTitle'],
                'translit_full_title': course['TranslitFullTitle'],
                'scripter': course.get('Scripter'),
                'description': course.get('Description'),
            })

            trail['course_hash'] = course_hash
            trail_hash = shake_128(json.dumps(trail, sort_keys=True).encode()).hexdigest(8)

            radar = trail.get('Radar')

            cur.execute('''
                INSERT INTO trail (
                    hash, course_hash, difficulty, meter, steps_type,
                    length_seconds, radar_notes, radar_taps_and_holds,
                    radar_jumps, radar_holds, radar_mines, radar_hands,
                    radar_rolls
                ) VALUES (
                    %(hash)s, %(course_hash)s, %(difficulty)s, %(meter)s,
                    %(steps_type)s, %(length_seconds)s, %(radar_notes)s,
                    %(radar_taps_and_holds)s, %(radar_jumps)s, %(radar_holds)s,
                    %(radar_mines)s, %(radar_hands)s, %(radar_rolls)s
                ) ON CONFLICT (hash) DO NOTHING
            ''', {
                'hash': trail_hash,
                'course_hash': course_hash,
                'difficulty': trail['Difficulty'],
                'meter': trail['Meter'],
                'steps_type': trail['StepsType'],
                'length_seconds': round(trail['LengthSeconds']),
                'radar_notes': radar['Notes'] if radar else None,
                'radar_taps_and_holds': radar['TapsAndHolds'] if radar else None,
                'radar_jumps': radar['Jumps'] if radar else None,
                'radar_holds': radar['Holds'] if radar else None,
                'radar_mines': radar['Mines'] if radar else None,
                'radar_hands': radar['Hands'] if radar else None,
                'radar_rolls': radar['Rolls'] if radar else None,
            })

        score = data['Score']
        tns = score['TapNoteScores']
        hns = score['HoldNoteScores']
        radar = score['Radar']

        music_rate = 1
        for modifier in score['Modifiers'].split(', '):
            m = re.fullmatch(r'(\d+\.\d+)xMusic', modifier)
            if m:
                music_rate = float(m.group(1))

        cur.execute('''
            INSERT INTO score (
                guid, username, steps_hash, trail_hash, game_mode,
                machine_guid, grade, score, percent_dp, survive_seconds,
                max_combo, modifiers, datetime, player_guid, disqualified, w1,
                w2, w3, w4, w5, miss, hit_mine, avoid_mine, checkpoint_miss,
                checkpoint_hit, let_go, held, missed_hold, radar_notes,
                radar_taps_and_holds, radar_jumps, radar_holds, radar_mines,
                radar_hands, radar_rolls, music_rate
            ) VALUES (
                %(guid)s, %(username)s, %(steps_hash)s, %(trail_hash)s,
                %(game_mode)s, %(machine_guid)s, %(grade)s, %(score)s,
                %(percent_dp)s, %(survive_seconds)s, %(max_combo)s,
                %(modifiers)s, %(datetime)s, %(player_guid)s, %(disqualified)s,
                %(w1)s, %(w2)s, %(w3)s, %(w4)s, %(w5)s, %(miss)s, %(hit_mine)s,
                %(avoid_mine)s, %(checkpoint_miss)s, %(checkpoint_hit)s,
                %(let_go)s, %(held)s, %(missed_hold)s, %(radar_notes)s,
                %(radar_taps_and_holds)s, %(radar_jumps)s, %(radar_holds)s,
                %(radar_mines)s, %(radar_hands)s, %(radar_rolls)s, %(music_rate)s
            ) ON CONFLICT (username, guid) DO NOTHING
        ''', {
            'guid': score['Guid'],
            'username': username,
            'steps_hash': steps_hash,
            'trail_hash': trail_hash,
            'game_mode': data['GameMode'],
            'machine_guid': data['MachineGuid'],
            'grade': score['Grade'],
            'score': score['Score'],
            'percent_dp': score['PercentDP'],
            'survive_seconds': round(score['SurviveSeconds']),
            'max_combo': score['MaxCombo'],
            'modifiers': score['Modifiers'],
            'datetime': score['DateTime'],
            'player_guid': score['PlayerGuid'],
            'disqualified': score['Disqualified'],
            'w1': tns['W1'],
            'w2': tns['W2'],
            'w3': tns['W3'],
            'w4': tns['W4'],
            'w5': tns['W5'],
            'miss': tns['Miss'],
            'hit_mine': tns['HitMine'],
            'avoid_mine': tns['AvoidMine'],
            'checkpoint_miss': tns['CheckpointMiss'],
            'checkpoint_hit': tns['CheckpointHit'],
            'let_go': hns['LetGo'],
            'held': hns['Held'],
            'missed_hold': hns['MissedHold'],
            'radar_notes': radar['Notes'],
            'radar_taps_and_holds': radar['TapsAndHolds'],
            'radar_jumps': radar['Jumps'],
            'radar_holds': radar['Holds'],
            'radar_mines': radar['Mines'],
            'radar_hands': radar['Hands'],
            'radar_rolls': radar['Rolls'],
            'music_rate': music_rate,
        })

        cur.close()
        cur.connection.commit()

    return 'ok', 200


@scores_blueprint.route('/scores/<guid>', methods=['DELETE'])
@require_logged_in
def delete_score(*, username, guid):
    if session['demo']:
        return jsonify(error='Removing scores is disabled for the demo account!'), 403

    with get_cursor() as cur:
        cur.execute('''
            DELETE FROM score
            WHERE username = %s AND guid = %s
        ''', (username, guid))

    return '', 204
