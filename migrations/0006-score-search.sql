ALTER TABLE song
ADD COLUMN search_vector tsvector NOT NULL GENERATED ALWAYS AS (
    to_tsvector('simple',
        pack || ' ' ||
        title || ' ' ||
        coalesce(subtitle, '') || ' ' ||
        coalesce(artist, '') || ' ' ||
        translit_title || ' ' ||
        coalesce(translit_subtitle, '') || ' ' ||
        coalesce(translit_artist, '')
    )
) STORED;
CREATE INDEX ON song USING GIN (search_vector);

ALTER TABLE steps
ADD COLUMN search_vector tsvector NOT NULL GENERATED ALWAYS AS (
    to_tsvector('simple',
        author_credit || ' ' || description
    )
) STORED;
CREATE INDEX ON steps USING GIN (search_vector);

ALTER TABLE course
ADD COLUMN search_vector tsvector NOT NULL GENERATED ALWAYS AS (
    to_tsvector('simple',
        full_title || ' ' || translit_full_title
    )
) STORED;
CREATE INDEX ON course USING GIN (search_vector);
