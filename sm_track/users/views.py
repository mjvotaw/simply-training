import jsonschema
from flask import Blueprint, jsonify, redirect, request, session, url_for
from psycopg2.errors import UniqueViolation

from sm_track.db import get_cursor
from sm_track.users.auth import check_password, hash_password
from sm_track.users.utils import require_logged_in


users_blueprint = Blueprint('users', __name__)


@users_blueprint.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    try:
        jsonschema.validate(data, schema={
            'type': 'object',
            'properties': {
                'username': {
                    'type': 'string',
                },
                'password': {
                    'type': 'string',
                },
            },
            'required': ['username', 'password'],
            'additionalProperties': False,
        }, format_checker=jsonschema.draft7_format_checker)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    username = data['username']
    password = data['password']

    with get_cursor() as cur:
        cur.execute('''
            SELECT password_hash, demo
            FROM account
            WHERE name = %s
        ''', (username,))
        row = cur.fetchone()

    if row is None or not check_password(password, row['password_hash']):
        return jsonify(error='Incorrect username or password!'), 401

    session['username'] = username
    session['demo'] = row['demo']
    session.permanent = True

    return redirect(url_for('users.settings'))


@users_blueprint.route('/logout', methods=['POST'])
def logout():
    session.pop('username', None)
    session.pop('demo', None)
    return '', 204


@users_blueprint.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    try:
        jsonschema.validate(data, schema={
            'properties': {
                'username': {
                    'type': 'string',
                    'minLength': 3,
                },
                'password': {
                    'type': 'string',
                    'minLength': 8,
                },
                'email': {
                    'type': 'string',
                    'format': 'email',
                },
            },
            'required': ['username', 'password', 'email'],
            'additionalProperties': False,
        }, format_checker=jsonschema.draft7_format_checker)
    except jsonschema.ValidationError as e:
        return jsonify(error=str(e)), 422

    data['password_hash'] = hash_password(data['password'])

    with get_cursor() as cur:
        try:
            cur.execute('''
                INSERT INTO account (name, password_hash, email)
                VALUES (%(username)s, %(password_hash)s, %(email)s)
            ''', data)
        except UniqueViolation:
            return jsonify(error='That user name is already taken, sorry!'), 422

    session['username'] = data['username']
    session['demo'] = False
    session.permanent = True

    return redirect(url_for('users.settings'))


@users_blueprint.route('/settings', methods=['GET', 'POST', 'DELETE'])
@require_logged_in
def settings(*, username):
    if request.method == 'GET':
        with get_cursor() as cur:
            cur.execute('''
                SELECT email, translit
                FROM account
                WHERE name = %s
            ''', (username,))
            row = cur.fetchone()

        return jsonify({
            'username': username,
            'email': row['email'],
            'translit': row['translit'],
        })
    elif request.method == 'DELETE':
        if session['demo']:
            return jsonify(error='The demo account cannot be deleted!'), 403

        with get_cursor() as cur:
            cur.execute('''
                DELETE FROM score
                WHERE username = %(username)s;

                DELETE FROM account
                WHERE name = %(username)s;
            ''', {
                'username': username,
            })

        session.pop('username', None)
        session.pop('demo', None)
        return '', 204
    else:
        if session['demo']:
            return jsonify(error='Demo account settings are locked!'), 403

        data = request.get_json()
        try:
            jsonschema.validate(data, schema={
                'properties': {
                    'newPassword': {
                        'type': 'string',
                        'minLength': 8,
                    },
                    'email': {
                        'type': 'string',
                        'format': 'email',
                    },
                    'translit': {
                        'type': 'boolean',
                    },
                },
                'required': ['email', 'translit'],
                'additionalProperties': False,
            }, format_checker=jsonschema.draft7_format_checker)
        except jsonschema.ValidationError as e:
            return jsonify(error=str(e)), 422

        with get_cursor() as cur:
            cur.execute('''
                UPDATE account
                SET
                    email = %(email)s,
                    translit = %(translit)s
                WHERE name = %(username)s
            ''', {
                'email': data['email'],
                'translit': data['translit'],
                'username': username,
            })

            if 'newPassword' in data:
                cur.execute('''
                    UPDATE account
                    SET password_hash = %(password_hash)s
                    WHERE name = %(username)s
                ''', {
                    'username': username,
                    'password_hash': hash_password(data['newPassword']),
                })

        return redirect(url_for('users.settings'))
