ALTER TABLE score ADD COLUMN music_rate REAL NOT NULL CHECK (music_rate > 0) DEFAULT 1;
ALTER TABLE score ALTER COLUMN music_rate DROP DEFAULT;

UPDATE score
SET music_rate = coalesce((
	SELECT split_part(modifier, 'x', 1)::real
	FROM regexp_split_to_table(score.modifiers, ', '::text) AS modifier
	WHERE modifier SIMILAR TO '%[0-9]+.[0-9]+xMusic%'
), 1);
