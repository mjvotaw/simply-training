from functools import wraps

from flask import session


def require_logged_in(fn):
    @wraps(fn)
    def _inner(**kwargs):
        try:
            username = session['username']
        except KeyError:
            return 'login required', 401

        return fn(username=username, **kwargs)

    return _inner
